package com.gmail.mame3619.lpo.core.manager;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.gmail.mame3619.lpo.core.util.MySQL;


public class StatusManager {
	
	public static boolean hasplayerata(String id){
		try{
		ResultSet rs = MySQL.getResult("SELECT * FROM PlayerData WHERE NAME= '" + id + "'");
		if(rs.next()){
			return rs.getString("NAME") != null;
		}
		return false;
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public static void createPlayer(String id){
		if(!hasplayerata(id)){
			MySQL.update("INSERT INTO `PlayerData` (`NAME`, `Win`, `Lose`, `Coin`, `VP`, `Permissions`, `KanaSkip`) VALUES ('"+ id + "', '0', '0', '0','0', 'Member', 'false');");
			System.out.println("[Player]" +id+ "を追加しました。");
		}
	}
	
	public static Integer getWin(String id){
		Integer i = Integer.valueOf(0);
		if(hasplayerata(id)){
			try {
			ResultSet rs = MySQL.getResult("SELECT * FROM PlayerData WHERE NAME= '"+ id +"'");
				if((rs.next()) && (Integer.valueOf(rs.getInt("Win")) == null)){}
				i = Integer.valueOf(rs.getInt("Win"));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else {
			createPlayer(id);
			getWin(id);
		}
		return i;
	}
	
	public static Integer getLose(String id){
		Integer i = Integer.valueOf(0);
		if(hasplayerata(id)){
			try {
			ResultSet rs = MySQL.getResult("SELECT * FROM PlayerData WHERE NAME= '"+ id +"'");
				if((rs.next()) && (Integer.valueOf(rs.getInt("Lose")) == null)){}
				i = Integer.valueOf(rs.getInt("Lose"));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else {
			createPlayer(id);
			getLose(id);
		}
		return i;
	}
	
	public static Integer getCoin(String id){
		Integer i = Integer.valueOf(0);
		if(hasplayerata(id)){
			try {
			ResultSet rs = MySQL.getResult("SELECT * FROM PlayerData WHERE NAME= '"+ id +"'");
				if((rs.next()) && (Integer.valueOf(rs.getInt("Coin")) == null)){}
				i = Integer.valueOf(rs.getInt("Coin"));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else {
			createPlayer(id);
			getCoin(id);
		}
		return i;
	}
	
	public static Integer getVP(String id){
		Integer i = Integer.valueOf(0);
		if(hasplayerata(id)){
			try {
			ResultSet rs = MySQL.getResult("SELECT * FROM PlayerData WHERE NAME= '"+ id +"'");
				if((rs.next()) && (Integer.valueOf(rs.getInt("VP")) == null)){}
				i = Integer.valueOf(rs.getInt("VP"));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else {
			createPlayer(id);
			getVP(id);
		}
		return i;
	}
	

	
	public static String getPermission(String id){
		String permission = "member";
		if(hasplayerata(id)){
			try {
			ResultSet rs = MySQL.getResult("SELECT * FROM PlayerData WHERE NAME = '" + id + "'");
			if(!rs.next()){}
				permission = rs.getString("Permissions");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return permission;
	}
	
	public static String getKanaSkip(String id){
		String kanaskip = "false";
		if(hasplayerata(id)){
			try {
			ResultSet rs = MySQL.getResult("SELECT * FROM PlayerData WHERE NAME = '" + id + "'");
			if(!rs.next()){}
				kanaskip = rs.getString("KanaSkip");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return kanaskip;
	}
	
	
	public static void setWins(String id, Integer wins){
		if(hasplayerata(id)){
			MySQL.update("UPDATE PlayerData SET Win = '"+wins + "' WHERE NAME= '"+id +"';");
		}else{
			createPlayer(id);
			setWins(id, wins);
		}
	}
	
	public static void setLoses(String id, Integer loses){
		if(hasplayerata(id)){
			MySQL.update("UPDATE PlayerData SET Lose = '"+loses + "' WHERE NAME= '"+id +"';");
		}else{
			createPlayer(id);
			setLoses(id, loses);
		}
	}
	
	public static void setPermission(String id, String permission){
		if(hasplayerata(id)){
			MySQL.update("UPDATE PlayerData SET Permissions = '"+ permission + "' WHERE NAME= '"+id +"';");
		}else{
			createPlayer(id);
			setPermission(id, permission);
		}
	}
	
	public static void setKanaSkip(String id, String kanaskip){
		if(hasplayerata(id)){
			MySQL.update("UPDATE PlayerData SET KanaSkip = '"+ kanaskip + "' WHERE NAME= '"+id +"';");
		}else{
			createPlayer(id);
			setKanaSkip(id, kanaskip);
		}
	}
	
	
	public static void setCoin(String id, Integer coins){
		if(hasplayerata(id)){
			MySQL.update("UPDATE PlayerData SET Coin = '" + coins + "' WHERE NAME= '"+id +"';");
		}else{
			createPlayer(id);
			setCoin(id, coins);
		}
	}
	
	public static void setVP(String id, Integer vp){
		if(hasplayerata(id)){
			MySQL.update("UPDATE PlayerData SET VP = '" + vp + "' WHERE NAME= '"+id +"';");
		}else{
			createPlayer(id);
			setVP(id, vp);
		}
	}
	
	public static void addWin(String id, Integer win){
		if(hasplayerata(id)){
			setWins(id, Integer.valueOf(getWin(id).intValue() +win.intValue()));
		}else {
			createPlayer(id);
			addWin(id, win);
		}
	}
	
	public static void addLose(String id, Integer loses){
		if(hasplayerata(id)){
			setLoses(id, Integer.valueOf(getWin(id).intValue() +loses.intValue()));
		}else {
			createPlayer(id);
			addLose(id, loses);
		}
	}
	
	public static void addCoin(String id, Integer coins){
		if(hasplayerata(id)){
			setCoin(id, Integer.valueOf(getCoin(id).intValue() +coins.intValue()));
		}else {
			createPlayer(id);
			addCoin(id, coins);
		}
	}
	
	public static void addVP(String id, Integer vp){
		if(hasplayerata(id)){
			setVP(id, Integer.valueOf(getVP(id).intValue() +vp.intValue()));
		}else {
			createPlayer(id);
			addVP(id, vp);
		}
	}
	
	public static void removeWin(String id, Integer win){
		if(hasplayerata(id)){
			setWins(id, Integer.valueOf(getWin(id).intValue() - win.intValue()));
		}else {
			createPlayer(id);
			removeWin(id, win);
		}
	}
	
	public static void removeLose(String id, Integer loses){
		if(hasplayerata(id)){
			setLoses(id, Integer.valueOf(getWin(id).intValue() - loses.intValue()));
		}else {
			createPlayer(id);
			removeLose(id, loses);
		}
	}
	
	public static void removeCoin(String id, Integer coins){
		if(hasplayerata(id)){
			setCoin(id, Integer.valueOf(getCoin(id).intValue() - coins.intValue()));
		}else {
			createPlayer(id);
			removeCoin(id, coins);
		}
	}
	
	public static void removeVP(String id, Integer vp){
		if(hasplayerata(id)){
			setVP(id, Integer.valueOf(getVP(id).intValue() - vp.intValue()));
		}else {
			createPlayer(id);
			removeVP(id, vp);
		}
	}




}
