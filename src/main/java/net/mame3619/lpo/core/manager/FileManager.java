package com.gmail.mame3619.lpo.core.manager;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.gmail.mame3619.lpo.core.listener.PlayerListener;
import com.gmail.mame3619.lpo.core.util.MySQL;

public class FileManager {


	public static File getMySQLFile(){
		return new File("plugins/LPOCore", "mysql.yml");
	}
	
	public static FileConfiguration getMySQLFileConfigration(){
		return YamlConfiguration.loadConfiguration(getMySQLFile());
	}

	
	public static void setStanderdMySQL(){
		FileConfiguration cfg = getMySQLFileConfigration();
		cfg.options().copyDefaults(true);
		cfg.addDefault("username", "PluginUser");
		cfg.addDefault("password", "LPOPL");
		cfg.addDefault("database", "PluginData");
		cfg.addDefault("host", "localhost");
		cfg.addDefault("port", "3306");
		cfg.addDefault("resourcepack", "none");
		
		
		try {
			cfg.save(getMySQLFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public static void readMySQL(){

		FileConfiguration cfg = getMySQLFileConfigration();
		MySQL.username = cfg.getString("username");
		MySQL.password = cfg.getString("password");
		MySQL.database = cfg.getString("database");
		MySQL.host = cfg.getString("host");
		MySQL.port = cfg.getString("port");
		PlayerListener.url = cfg.getString("resourcepack");
		
		
	}
}
