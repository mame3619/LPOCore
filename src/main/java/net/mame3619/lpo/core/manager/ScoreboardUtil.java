package com.gmail.mame3619.lpo.core.manager;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class ScoreboardUtil {
	
	public static Scoreboard getMainScoreboard(){
		return Bukkit.getServer().getScoreboardManager().getMainScoreboard();
	}
	
	public static String getteam(Player player){
		Team team = getPlayerTeam(getMainScoreboard(), player);
		String teme = "";
		if(team == null){
			
		}
		else{
			String teamname = team.getName();
			if(teamname.equalsIgnoreCase("team_blue")){
				teme = "blue";
			}
			if(teamname.equalsIgnoreCase("team_red")){
				teme = "red";
			}
		}
		return teme;
	}
	
	   public static Team getPlayerTeam(Scoreboard scoreboard, Player player) {
		     Set<Team> teams = scoreboard.getTeams();
		     for (Team team : teams) {
		       if (team != null) {
		         for (OfflinePlayer p : team.getPlayers()) {
		           if (p.getName().equalsIgnoreCase(player.getName())) {
		             return team;
		           }
		         }
		       }
		     }
		     return null;
		   }
}
