package com.gmail.mame3619.lpo.core.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import com.gmail.mame3619.lpo.core.listener.BotListener;
import com.gmail.mame3619.lpo.core.util.MySQL;


public class BanManager {
	public static void ban(String uuid, String playername, String Reason, long seconds, String bannedby){
		long end = 0;
		if(seconds == -1){
			end = -1;
		}else{
			long current = System.currentTimeMillis();
			long millis = seconds*1000;
			end = current+millis;
		}

		Calendar calendar = Calendar.getInstance();
		int year  = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DATE);
	    int hour = calendar.get(Calendar.HOUR_OF_DAY);
	    int minute = calendar.get(Calendar.MINUTE);
	    int second = calendar.get(Calendar.SECOND);
	    String date = year + "/" + month + "/" + day + "  " + hour + ":" + minute + "." + second;

		MySQL.update("INSERT INTO `bannedplayers` (`PlayerName`, `UUID`, `Reason`, `Bannedby`, `End`, `Date`, `DiscordID`) VALUES ('"+ playername+"', '"+uuid+"', '"+Reason+"', '"+bannedby+"', '"+end+"', '"+date+"','0')");
		if(Bukkit.getPlayer(playername) !=null){
				Bukkit.getPlayer(playername).kickPlayer(ChatColor.RED + "" + ChatColor.BOLD + "あなたはBANされました。\n"+
														"\n" +
														ChatColor.RESET + ChatColor.AQUA + "理由: "+ ChatColor.RESET + getReason(uuid)+ "\n"+
														"\n" +
												ChatColor.AQUA + "期限: " +ChatColor.RESET +  getRemainingTime(uuid) + "\n"+
														"\n" +
														ChatColor.AQUA + "BANした運営: " + ChatColor.RESET + getBannedby(uuid) + "\n"+
														"\n" +
														ChatColor.RESET+ "                          "+ getDate(uuid));
		}
	    BotListener.banDiscord(playername,uuid , getReason(uuid), getDate(uuid), getRemainingTime(uuid), getBannedby(uuid));
	}
	
	public static void unban(String playername , String unbandby,String uuid){
		String DiscordID = getDiscordID(uuid);
		BotListener.unbanDiscord(playername, unbandby, DiscordID);
		MySQL.update("DELETE FROM BannedPlayers WHERE `UUID`= '"+ uuid +"'");

		
		
	}
	
	public static boolean isBanned(String uuid){
		ResultSet rs = MySQL.getResult("SELECT End FROM BannedPlayers WHERE `UUID`= '"+ uuid +"'");
		try {
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static String getReason(String uuid){
		ResultSet rs = MySQL.getResult("SELECT * FROM BannedPlayers WHERE `UUID`= '"+ uuid +"'");
		try {
			while (rs.next()){
				return rs.getString("Reason");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
		
	}
	
	public static String getBannedby(String uuid){
		ResultSet rs = MySQL.getResult("SELECT * FROM BannedPlayers WHERE `UUID`= '"+ uuid +"'");
		try {
			while (rs.next()){
				return rs.getString("Bannedby");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
		
		
	}
	
	public static String getDate(String uuid){
		ResultSet rs = MySQL.getResult("SELECT * FROM BannedPlayers WHERE `UUID`= '"+ uuid +"'");
		try {
			while (rs.next()){
				return rs.getString("Date");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
		
		
	}
	
	public static String getDiscordID(String uuid){
		ResultSet rs = MySQL.getResult("SELECT * FROM BannedPlayers WHERE `UUID`= '"+ uuid +"'");
		try {
			while (rs.next()){
				return rs.getString("DiscordID");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
		
		
	}
	
	public static void setDiscordID(String id, String ID){
			MySQL.update("UPDATE BannedPlayers SET DiscordID = '"+ ID + "' WHERE `PlayerName`= '"+id +"';");
	}

	public static Long getEnd(String uuid){
		ResultSet rs = MySQL.getResult("SELECT * FROM BannedPlayers WHERE `UUID`= '"+ uuid +"'");
		try {
			while (rs.next()){
				return rs.getLong("End");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static List<String> getBannedPlayers() {
		List<String> list = new ArrayList<String>();
		ResultSet rs = MySQL.getResult("SELECT * FROM BannedPlayers");
		try {
			while(rs.next()){
				list.add(rs.getString("Playername"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
		
		
	}
	
	public static String getRemainingTime(String uuid){
		long current = System.currentTimeMillis();
		long end = getEnd(uuid);
		if(end == -1) {
			return "無期限";
		}
		long millis = end - current;
		
		long seconds = 0;
		long minutes = 0;
		long hours = 0;
		long days = 0;
		long weeks = 0;
		while(millis > 1000){
			millis-=1000;
			seconds++;
		}
		while (seconds > 60){
			seconds-=60;
			minutes++;
		}
		while (minutes > 60){
			minutes-=60;
			hours++;
		}
		while (hours > 24) {
			hours-=24;
			days++;
		}
		while(days > 7) {
			days-=7;
			weeks++;
		}
		return weeks + "週間" + days +"日" + hours + "時間" + minutes + "分" + seconds + "秒";
	}


}
