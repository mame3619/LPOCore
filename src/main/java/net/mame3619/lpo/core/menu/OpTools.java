package com.gmail.mame3619.lpo.core.menu;

import java.util.ArrayList;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.gmail.mame3619.lpo.core.manager.StatusManager;


public class OpTools implements Listener{
		
		   public static void c(Material Material, Inventory inv, int Slot, String name, String lore)
		   {
		     ItemStack item = new ItemStack(Material, 1);
		     ItemMeta meta = item.getItemMeta();
		     meta.setDisplayName(name);
		     ArrayList<String> Lore = new ArrayList<>();
		     Lore.add(lore);
		     meta.setLore(Lore);
		     item.setItemMeta(meta);
		     
		     inv.setItem(Slot, item);
		   }
		   
		   public static void playerinfo(Inventory inv, int Slot, Player player) {
			   ItemStack item = new ItemStack(Material.SKULL_ITEM);
		     SkullMeta meta = (SkullMeta)item.getItemMeta();
		     meta.setOwner(player.getName());
		     meta.setDisplayName(ChatColor.WHITE +"" + ChatColor.BOLD + player.getName());
		     meta.setOwner(player.getName());
		     Location location = player.getLocation();
		     double x = location.getX();
		     double y = location.getY();
		     double z = location.getZ();
		     ArrayList<String> Lore = new ArrayList<>();
		     Lore.add(ChatColor.GREEN + "Location: " + ChatColor.WHITE + ChatColor.BOLD + x + y + z);
		     meta.setLore(Lore);
		     item.setItemMeta(meta);	     
		     inv.setItem(Slot, item);
		   }
		   
		   public static void SlotItems(Material Material, Inventory inv, int Slot, String name) {
		     ItemStack item = new ItemStack(Material, 1);
		     ItemMeta meta = item.getItemMeta();
		     meta.setDisplayName(name);
		     item.setItemMeta(meta);
		     inv.setItem(Slot, item);
		   }
		   
		   public static void openMenu(Player player){
			  
		    Inventory menu = Bukkit.createInventory(player, 27, ChatColor.DARK_GRAY + "Your Menu");
		    for(Player p : Bukkit.getOnlinePlayers()){
		    	
		    }
		    SlotItems(Material.COMPASS, menu, 9, ChatColor.BOLD + "Server移動");
		    SlotItems(Material.DIAMOND, menu, 11, ChatColor.BOLD + "クラン設定");
		    SlotItems(Material.ANVIL, menu, 15, ChatColor.BOLD + "パーティー設定");
		    SlotItems(Material.SKULL_ITEM, menu, 17, ChatColor.BOLD + "Friends");
		     player.openInventory(menu);
		   }
		   
			@EventHandler
			public void onClick(InventoryClickEvent e){
				Player player = (Player)e.getWhoClicked();
				ItemStack clicked = e.getCurrentItem();
				Inventory inv = e.getInventory();
				if(inv.getName().equals("Your Menu")) {
					e.setCancelled(true);
					return;
				}
			}

}
