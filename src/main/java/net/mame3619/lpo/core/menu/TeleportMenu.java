package com.gmail.mame3619.lpo.core.menu;

import java.util.ArrayList;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.avaje.ebean.meta.MetaAutoFetchStatistic;
import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.manager.StatusManager;


public class TeleportMenu implements Listener{
		

		   public static void playerinfo(Inventory inv, int Slot, Player player) {

             ItemStack item = new ItemStack(Material.SKULL_ITEM,1);
		     SkullMeta meta = (SkullMeta)item.getItemMeta();
             item.setDurability((short)3);
		     meta.setOwner(player.getName());
		     meta.setDisplayName(player.getName());
		     meta.setOwner(player.getName());
		     Location location = player.getLocation();
		     double x = location.getX();
		     double y = location.getY();
		     double z = location.getZ();
		     ArrayList<String> Lore = new ArrayList<>();
		     Lore.add(ChatColor.GREEN + "Location: " + ChatColor.WHITE + ChatColor.BOLD + x + y + z);
		     meta.setLore(Lore);
		     item.setItemMeta(meta);	     
		     inv.setItem(Slot, item);
		   }
		   public static void openMenu(Player player){
			   Inventory menu = null;
			  if(Bukkit.getOnlinePlayers().size() <= 9){
		     menu = Bukkit.createInventory(player, 9, ChatColor.DARK_GRAY + "Select Player");
			  }
			  else if(Bukkit.getOnlinePlayers().size() <= 18){
				  menu = Bukkit.createInventory(player, 18, ChatColor.DARK_GRAY + "Select Player");
			  }
			  else if(Bukkit.getOnlinePlayers().size() <= 27){
				  menu = Bukkit.createInventory(player, 27, ChatColor.DARK_GRAY + "Select Player");
			  }
			  else if(Bukkit.getOnlinePlayers().size() <= 36){
				  menu = Bukkit.createInventory(player, 36, ChatColor.DARK_GRAY + "Select Player");
			  }
			  else if(Bukkit.getOnlinePlayers().size() <= 45){
				  menu = Bukkit.createInventory(player, 45, ChatColor.DARK_GRAY + "Select Player");
			  }
			  else if(Bukkit.getOnlinePlayers().size() <= 54){
				  menu = Bukkit.createInventory(player, 54, ChatColor.DARK_GRAY + "Select Player");
			  }
		    int slot = 0;
		    for(Player p : Bukkit.getOnlinePlayers()){
		    	playerinfo(menu, slot, p);
		    	slot += 1;
		    }
		     player.openInventory(menu);
		   }
		   
			@EventHandler
			public void onClick(InventoryClickEvent e){
				Player player = (Player)e.getWhoClicked();
				ItemStack clicked = e.getCurrentItem();
				Inventory inv = e.getInventory();
				if(inv.getName().equals(ChatColor.DARK_GRAY + "Select Player")) {
					e.setCancelled(true);
				      player.closeInventory();
				      String name = clicked.getItemMeta().getDisplayName();
				      Player teleportplayer = Bukkit.getPlayer(name);
				      player.teleport(teleportplayer);
				      player.sendMessage(LPOCore.systemprefix + ChatColor.DARK_AQUA + teleportplayer.getName() + "にテレポートしました");
					return;
				}
			}

}
