package com.gmail.mame3619.lpo.core.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import com.gmail.mame3619.lpo.core.LPOCore;

public class MySQL {
	public static String username;
	public static String password;
	public static String database;
	public static String host;
	public static String port;
	public static Connection connection;
	
	public static void connect(){
		if(!isConnected()){
			try {
				connection = DriverManager.getConnection("jdbc:mysql://"+ host + ":" + port+"/"+ database +"?autoReconnect=true", username, password);
				System.out.println("[MySQL] Connect MySQL Server!");
				LPOCore.Connect = true;
			} catch (SQLException e) {
				System.out.println("[MySQL] Can't Connect MySQL Server!  Error: " + e.getMessage());
			}
		}
	}
	public static void close(){
		if(isConnected()){
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			Bukkit.getConsoleSender().sendMessage(ChatColor.AQUA + "[MySQL] DisConnect MySQL Server!");
		}
	}

	private static boolean isConnected() {
		return connection != null;
	}
	
	public static void CreateTable() {
		if(isConnected()){
			try {
				connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS PlayerData ( `NAME` VARCHAR(100) , `Win` INT , `Lose` INT , `Coin` INT ,`VP` INT,  `Permissions` VARCHAR(20) , `KanaSkip` VARCHAR(5))");
				connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS BannedPlayers ( `PlayerName` VARCHAR(100) , `UUID` VARCHAR(100) , `Reason` VARCHAR(100) , `Bannedby` VARCHAR(100) , `End` VARCHAR(100) , `Date` VARCHAR(100) , `DiscordID` VARCHAR(100) )");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void update(String qry){
		if(isConnected()){
			try {
				connection.createStatement().executeUpdate(qry);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static ResultSet getResult(String qry){
		if(isConnected()){
			try {
				return connection.createStatement().executeQuery(qry);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
