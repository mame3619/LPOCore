package com.gmail.mame3619.lpo.core.util;

import java.util.ArrayList;
import java.util.List;

public enum BanUnit {
	
	SECOND("Second(s)", 1, "sec"),
	MINUTE("Minute(s)", 60, "min"),
	HOUR("Hour(s)", 60*60, "hour"),
	DAY("Day(s)", 24*60*60, "day"),
	WEEK("Week(s)", 7*24*60*60, "week"),
	MONTH("Month(es)", 30*24*60*60, "month"),
	YEAR("Year(s)", 365*24*60*60, "year");
	
	private String name;
	private long toSecond;
	private String shortcut;
	
	private BanUnit(String name, long toSecond, String shortcut){
		this.name = name;
		this.toSecond = toSecond;
		this.shortcut = shortcut;
	}
	
	public long getToSecond(){
		return toSecond;
	}
	
	public String getName(){
		return name;
	}
	
	public String getShortcut(){
		return shortcut;
	}

	public static List<String> getUnitsAsString() {
		List<String> units = new ArrayList<String>();
		for(BanUnit unit : BanUnit.values()) {
			units.add(unit.getShortcut().toLowerCase());
		}
		return units;
	}
	
	public static BanUnit getUnit(String unit){
		for(BanUnit units : BanUnit.values()) {
			if(units.getShortcut().toLowerCase().equals(unit.toLowerCase())){
				return units;
			}
		}
		return null;
	}
}
