package com.gmail.mame3619.lpo.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.manager.StatusManager;

public class GiveCoinCommand implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("givecoin")){
			String permission = StatusManager.getPermission(sender.getName());
			if(permission.equalsIgnoreCase("Owner") || permission.equalsIgnoreCase("DeveloperPlus") || permission.equalsIgnoreCase("Administrator") || permission.equalsIgnoreCase("BuilderMaster")){

			if(args.length == 2){
				String args0 = args[0];
			int coins;
			try{
			coins = Integer.valueOf(args[1]);
			}catch (NumberFormatException e) {
				sender.sendMessage(LPOCore.coinprefix + "<Value> must be a number");
				return true;
			}
			@SuppressWarnings("deprecation")
			Player player = Bukkit.getOfflinePlayer(args0).getPlayer();
			StatusManager.addCoin(args0, coins);
			sender.sendMessage(LPOCore.coinprefix + ChatColor.BLUE + args0 + "に" + coins+ "coin(s) 追加しました。\n"+
							   LPOCore.coinprefix + ChatColor.BLUE +args0 +  "の残高: " +ChatColor.WHITE +  StatusManager.getCoin(sender.getName()) + ChatColor.BLUE + "Coin(s)");
				for(Player p : Bukkit.getServer().getOnlinePlayers()){
					if(args0.equalsIgnoreCase(p.getName())){
						player.sendMessage(LPOCore.coinprefix + ChatColor.BLUE + sender.getName() + "に" + coins+ "coin(s) 追加されました。\n"+
								   		   LPOCore.coinprefix + ChatColor.BLUE + "残高: " +ChatColor.WHITE +  StatusManager.getCoin(args0) + ChatColor.BLUE + "Coin(s)");
					}
					else{
						
					}
				}
			}else{
				sender.sendMessage(LPOCore.coinprefix + ChatColor.RED + "/givecoin <PlayerName> <Value>");
			}
			}else{
				sender.sendMessage(LPOCore.warmprefix + ChatColor.RED + "You don't have Permission.");
			}
		}
		return false;
	}
}
