package com.gmail.mame3619.lpo.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.manager.StatusManager;

public class SendCoinCommand implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("sendcoin")){
			if(args.length <= 2){
				String args0 = args[0];
				int coins;
				try{
				coins = Integer.valueOf(args[1]);
				}catch (NumberFormatException e) {
					sender.sendMessage(LPOCore.coinprefix + "<Value> must be a number");
					return true;
				}
				int sendernowcoins = StatusManager.getCoin(sender.getName());
				@SuppressWarnings("deprecation")
				Player player = Bukkit.getOfflinePlayer(args0).getPlayer();
				if(sendernowcoins - coins < 0){
					sender.sendMessage(LPOCore.coinprefix + "");
				}
				else {
					StatusManager.addCoin(args0, coins);
					StatusManager.removeCoin(sender.getName(), coins);
					sender.sendMessage(LPOCore.coinprefix + ChatColor.BLUE + args0 + "に" + coins+ "coin(s) 送金しました。\n"+
									   LPOCore.coinprefix + ChatColor.BLUE + "残り残高: " +ChatColor.WHITE +  StatusManager.getCoin(sender.getName()) + ChatColor.BLUE + "Coin(s)");
					for(Player p : Bukkit.getServer().getOnlinePlayers()){
						if(args0.equalsIgnoreCase(p.getName())){
							player.sendMessage(LPOCore.coinprefix + ChatColor.BLUE + args0 + "から" + coins+ "coin(s) 着金しました。\n"+
									   LPOCore.coinprefix + ChatColor.BLUE + "残高: " +ChatColor.WHITE +  StatusManager.getCoin(args0) + ChatColor.BLUE + "Coin(s)");
						}
					}
				}
			}else{
				sender.sendMessage(LPOCore.coinprefix + ChatColor.RED + "/sendcoin <PlayerName> <Value>");
			}
		}
		return false;
	}
}
