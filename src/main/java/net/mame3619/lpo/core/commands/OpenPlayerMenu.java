package com.gmail.mame3619.lpo.core.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.mame3619.lpo.core.menu.PlayerMenu;
import com.gmail.mame3619.lpo.core.menu.TeleportMenu;

public class OpenPlayerMenu implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("menu")){
			Player player = (Player)sender;
			PlayerMenu.openMenu(player);
		}
		return false;
	}

}
