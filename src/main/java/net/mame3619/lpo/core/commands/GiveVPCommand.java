package com.gmail.mame3619.lpo.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.manager.StatusManager;

public class GiveVPCommand implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("givevp")){
			String permission = StatusManager.getPermission(sender.getName());
			if(permission.equalsIgnoreCase("Owner") || permission.equalsIgnoreCase("DeveloperPlus") || permission.equalsIgnoreCase("Administrator") || permission.equalsIgnoreCase("BuilderMaster")){

			if(args.length == 2){
				String args0 = args[0];
			int coins;
			try{
			coins = Integer.valueOf(args[1]);
			}catch (NumberFormatException e) {
				sender.sendMessage(LPOCore.vpprefix + "<Value> must be a number");
				return true;
			}
			@SuppressWarnings("deprecation")
			Player player = Bukkit.getOfflinePlayer(args0).getPlayer();
			StatusManager.addVP(args0, coins);
			sender.sendMessage(LPOCore.vpprefix + ChatColor.BLUE + args0 + "に" + coins+ "vp(s) 追加しました。\n"+
							   LPOCore.vpprefix + ChatColor.BLUE +args0 +  "の残高: " +ChatColor.WHITE +  StatusManager.getVP(args0) + ChatColor.BLUE + "VP(s)");
				for(Player p : Bukkit.getServer().getOnlinePlayers()){
					if(args0.equalsIgnoreCase(p.getName())){
						player.sendMessage(LPOCore.vpprefix + ChatColor.BLUE + sender.getName() + "に" + coins+ "vp(s) 追加されました。\n"+
								   		   LPOCore.vpprefix + ChatColor.BLUE + "残高: " +ChatColor.WHITE +  StatusManager.getCoin(args0) + ChatColor.BLUE + "VP(s)");
					}
					else{
						
					}
				}
			}else{
				sender.sendMessage(LPOCore.vpprefix + ChatColor.RED + "/givevp <PlayerName> <Value>");
			}
			}else{
				sender.sendMessage(LPOCore.warmprefix + ChatColor.RED + "You don't have Permission.");
			}
		}
		return false;
	}
}
