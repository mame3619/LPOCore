package com.gmail.mame3619.lpo.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.manager.StatusManager;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

public class SystemCommands implements CommandExecutor{
	
	  public static Plugin plugin;
	  
	  public SystemCommands(Plugin plugin){
	    SystemCommands.plugin = plugin;
	  }

	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("system")){

			Player player = (Player)sender;
			String playername = sender.getName();
			String permission = StatusManager.getPermission(playername);
			if(permission.equalsIgnoreCase("Owner") || permission.equalsIgnoreCase("DeveloperPlus") || permission.equalsIgnoreCase("Administrator") || permission.equalsIgnoreCase("BuilderMaster")){
				if(args.length == 1){
					String args0 = args[0];
					if(args0.equalsIgnoreCase("reload")){
						sender.sendMessage(LPOCore.systemprefix + ChatColor.GOLD + "/system reload " +ChatColor.LIGHT_PURPLE + "<PluginName> || all ");
					}
					if(args0.equalsIgnoreCase("enable")){
						sender.sendMessage(LPOCore.systemprefix + ChatColor.GOLD + "/system enable " +ChatColor.LIGHT_PURPLE + "<PluginName>");
					}
					if(args0.equalsIgnoreCase("disable")){
						sender.sendMessage(LPOCore.systemprefix + ChatColor.GOLD + "/system disable " +ChatColor.LIGHT_PURPLE + "<PluginName>");
					}
					if(args0.equalsIgnoreCase("restart")){
						sender.sendMessage(LPOCore.systemprefix + ChatColor.GOLD + "/system restart " +ChatColor.LIGHT_PURPLE + "<Reason>");
					}
					if(args0.equalsIgnoreCase("permission")){
						sender.sendMessage(LPOCore.systemprefix + ChatColor.GOLD + "/system permission" + ChatColor.GREEN +  " [PlayerName]"+ ChatColor.LIGHT_PURPLE + " <PermissionName> ");
					}
				}
				if(args.length == 2){
					String args0 = args[0];
					String args1 = args[1];
					if(args0.equalsIgnoreCase("reload")){
						if(args1.equalsIgnoreCase("all")){
							Bukkit.broadcastMessage(LPOCore.systemprefix + "reload中...");
							Bukkit.getServer().reload();
							Bukkit.broadcastMessage(LPOCore.systemprefix + "reload完了");
						}else{
							Plugin plugin = Bukkit.getPluginManager().getPlugin(args1);
							if(plugin == null){
								sender.sendMessage(LPOCore.systemprefix + ChatColor.GOLD + "Plugin名: " + args1 +"は存在しません。");
							}else{
								Bukkit.broadcastMessage(LPOCore.systemprefix + ChatColor.GREEN + "Plugin名: " + args1 + " reload中...");
								Bukkit.getPluginManager().disablePlugin(plugin);
								Bukkit.getPluginManager().enablePlugin(plugin);
								Bukkit.broadcastMessage(LPOCore.systemprefix + ChatColor.GREEN + "Plugin名: "+ args1 + " reload完了");
							}
						}
					}
					if(args0.equalsIgnoreCase("disable")){
						Plugin plugin = Bukkit.getPluginManager().getPlugin(args1);
						if(plugin == null){
							sender.sendMessage(LPOCore.systemprefix + ChatColor.GOLD + "Plugin名: " + args1 +"は存在しません。");
						}else{
								Bukkit.broadcastMessage(LPOCore.systemprefix + ChatColor.GREEN + "Plugin名: " + args1 + " を無効化中...");
								Bukkit.getPluginManager().disablePlugin(plugin);
								Bukkit.broadcastMessage(LPOCore.systemprefix + ChatColor.GREEN + "Plugin名: "+ args1 + " を無効化完了");
						}
					}
					if(args0.equalsIgnoreCase("enable")){
						Plugin plugin = Bukkit.getPluginManager().getPlugin(args1);
						if(plugin == null){
							sender.sendMessage(LPOCore.systemprefix + ChatColor.GOLD + "Plugin名: " + args1 +"は存在しません。");
						}else{
							Bukkit.broadcastMessage(LPOCore.systemprefix + ChatColor.GREEN + "Plugin名: " + args1 + " を有効化中...");
							Bukkit.getPluginManager().enablePlugin(plugin);
							Bukkit.broadcastMessage(LPOCore.systemprefix + ChatColor.GREEN + "Plugin名: "+ args1 + " を有効化完了");
						}
					}
					if(args0.equalsIgnoreCase("restart")){
						TextComponent tc = new TextComponent();
						tc.setText("実行する");
						tc.setColor(net.md_5.bungee.api.ChatColor.GREEN);
						tc.setBold(true);
						tc.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("再起動を１０秒後に実施します。").create()));
						tc.setClickEvent(new ClickEvent(net.md_5.bungee.api.chat.ClickEvent.Action.RUN_COMMAND, "/res " + args1 + " " + sender.getName()));
						
						sender.sendMessage(LPOCore.systemprefix + "再起動実行確認。\n"+
										   LPOCore.systemprefix + "理由: " + args1);
						player.spigot().sendMessage(tc);
					
					}else {
						Unknown(sender);
					}
					
				}
				if(args.length == 3){
					String args0 = args[0];
					String args1 = args[1];
					String args2 = args[2];
					if(args0.equalsIgnoreCase("permission")){
						if(args2.equalsIgnoreCase("Owner") || args2.equalsIgnoreCase("Developer") || args2.equalsIgnoreCase("Administrator") || args2.equalsIgnoreCase("BuilderMaster") || args2.equalsIgnoreCase("Moderator") || args2.equalsIgnoreCase("BuilderPlus") || args2.equalsIgnoreCase("YouTube") || args2.equalsIgnoreCase("Builder") || args2.equalsIgnoreCase("Helper") || args2.equalsIgnoreCase("TextureCreator") || args2.equalsIgnoreCase("SpecialThanks") || args2.equalsIgnoreCase("Member")){
							if(args1.equalsIgnoreCase("mame3619") || args1.equalsIgnoreCase("nekonyan0322")){
								sender.sendMessage(LPOCore.systemprefix + "そのプレイヤーの権限を変更することはできません。");
							}else{
							StatusManager.setPermission(args1, args2);
							}
						}else{
							sender.sendMessage(LPOCore.systemprefix + "そのような権限はありません。");	
						}
					}else{
						Unknown(sender);
					}
				}else{	
					Unknown(sender);
				}
			}else{
				sender.sendMessage(LPOCore.warmprefix + ChatColor.RED + "You don't have Permission.");
			}
		}
		return false;
	}
	
	private void Unknown(CommandSender sender) {
		sender.sendMessage(ChatColor.GRAY + "----------------------------------------------------------------------\n" +
				   ChatColor.DARK_GREEN + "/system - SystemCommands\n" +
				   ChatColor.GOLD + "/system permission" + ChatColor.GREEN +  " [PlayerName]"+ ChatColor.LIGHT_PURPLE + " <PermissionName> "+ChatColor.GRAY + "- Setup Permissions\n"+
				   ChatColor.GOLD + "/system infomessage " + ChatColor.LIGHT_PURPLE + "<Message> "+ChatColor.GRAY + "- Send Server Info Message\n"+
				   ChatColor.GOLD + "/system warmmessage " + ChatColor.LIGHT_PURPLE + "<Message> "+ChatColor.GRAY + "- Send Server Warm Message\n"+
				   ChatColor.GOLD + "/system reload " +ChatColor.LIGHT_PURPLE + "<PluginName> || all "+  ChatColor.GRAY + "- Reload Plugin || all Plugin Reload\n"+
				   ChatColor.GOLD + "/system restart " + ChatColor.GRAY + "- Server ReStart\n");
	}

}
