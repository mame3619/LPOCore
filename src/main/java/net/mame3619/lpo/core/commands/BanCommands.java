package com.gmail.mame3619.lpo.core.commands;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.listener.BotListener;
import com.gmail.mame3619.lpo.core.manager.BanManager;
import com.gmail.mame3619.lpo.core.manager.StatusManager;
import com.gmail.mame3619.lpo.core.util.BanUnit;


public class BanCommands implements CommandExecutor{
	
	public static Plugin plugin;
	
	public BanCommands(LPOCore lpoCore) {
		BanCommands.plugin = lpoCore;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("ban")){			
			String permission = StatusManager.getPermission(sender.getName());
		if(permission.equalsIgnoreCase("Owner") || permission.equalsIgnoreCase("DeveloperPlus") || permission.equalsIgnoreCase("Administrator") || permission.equalsIgnoreCase("BuilderMaster") || permission.equalsIgnoreCase("BuilderPlus") || permission.equalsIgnoreCase("Builder")){

			if(args.length == 2){
				String playername  =args[0];
				if(BanManager.isBanned(getUUID(playername))) {
					sender.sendMessage(LPOCore.banprefix + "This Player is banned");
					return true;
				}
				String reason = args[1];
				BanManager.ban(getUUID(playername), playername, reason, -1, sender.getName());
				Bukkit.broadcastMessage(LPOCore.banprefix + "PlayerID: "+  playername+"を、無期限でBANしました。");
				Bukkit.broadcastMessage(LPOCore.banprefix + "Reason: " + reason);
				Bukkit.broadcastMessage(LPOCore.banprefix + "Bannedby: " + sender.getName());
				
				return true;
			}
			if(args.length == 4){
				String playername = args[0];
				long value;
				try{
				value = Integer.valueOf(args[2]);
				}catch (NumberFormatException e) {
					sender.sendMessage(LPOCore.banprefix + "<Value> must be a number");
					return true;
				}
				if(value >= 500) {
					sender.sendMessage(LPOCore.banprefix + ChatColor.RED + "500以上の数値の指定はできません。");
					return true;
				}
				String unitString = args[3];
				String reason = "";
				reason = args[1];
				List<String> unitList = BanUnit.getUnitsAsString();
				if(unitList.contains(unitString.toLowerCase())){
					BanUnit unit = BanUnit.getUnit(unitString);
					long seconds = value * unit.getToSecond();
					BanManager.ban(getUUID(playername), playername, reason, seconds, sender.getName());
					Bukkit.broadcastMessage(LPOCore.banprefix + "PlayerID: "+  playername+"を、"+ value+ unitString + "BANしました。");
					Bukkit.broadcastMessage(LPOCore.banprefix + "Reason: " + reason);
					Bukkit.broadcastMessage(LPOCore.banprefix + "Bannedby: " + sender.getName());
					return true;
				}
				sender.sendMessage(LPOCore.banprefix + "This <unit> does not exist!");
				return true;
				
			}
			else {
			sender.sendMessage(LPOCore.banprefix + ChatColor.RED + "/ban <PlayerName> <Reason> [<Value> <Unit>]");
			}
		}else {
			
		}
		
			return true;
		}
		if(cmd.getName().equalsIgnoreCase("check")){
			String permission = StatusManager.getPermission(sender.getName());
		if(permission.equalsIgnoreCase("Owner") || permission.equalsIgnoreCase("DeveloperPlus") || permission.equalsIgnoreCase("Administrator") || permission.equalsIgnoreCase("BuilderMaster")){

			if(args.length == 1){
				if(args[0].equalsIgnoreCase("list")){
					List<String> list = BanManager.getBannedPlayers();
					if(list.size() == 0){
						sender.sendMessage(LPOCore.banprefix + "BANされているプレイヤーはいません。");
					}
					sender.sendMessage(LPOCore.banprefix + "---------- Ban-List ----------");
					for(String allBanned : BanManager.getBannedPlayers()){
						sender.sendMessage(LPOCore.banprefix + allBanned + "( 理由:" + BanManager.getReason(getUUID(allBanned)) + " )");
					}
					return true;
				}
				String playername = args[0];
				sender.sendMessage(LPOCore.banprefix + "---------- Ban-Info ----------");
				sender.sendMessage(LPOCore.banprefix + "Name: " + playername);
				sender.sendMessage(LPOCore.banprefix + "Banned: " + BanManager.isBanned(getUUID(playername)));
				if(BanManager.isBanned(getUUID(playername))){

					sender.sendMessage(LPOCore.banprefix + "Reason: " + BanManager.getReason(getUUID(playername)));
					sender.sendMessage(LPOCore.banprefix + "Remaining Time: " + BanManager.getRemainingTime(getUUID(playername)));
				}
				return true;
			}
			sender.sendMessage(LPOCore.banprefix + "/check (list) <PlayerName>");
		}else {
			sender.sendMessage(LPOCore.warmprefix + ChatColor.RED + "You don't have Permission.");
		}
		}
		if(cmd.getName().equalsIgnoreCase("unban")){
			String permission = StatusManager.getPermission(sender.getName());
		if(permission.equalsIgnoreCase("Owner") || permission.equalsIgnoreCase("DeveloperPlus") || permission.equalsIgnoreCase("Administrator") || permission.equalsIgnoreCase("BuilderMaster")){

			if(args.length == 1){
				String playername = args[0];
				if(!BanManager.isBanned(getUUID(playername))){
					sender.sendMessage(LPOCore.banprefix + "そのプレイヤーはBANされていません。");
				}
				else{
				BanManager.unban(playername, sender.getName(), getUUID(playername));
				Bukkit.broadcastMessage(LPOCore.banprefix + playername +"のBANを解除しました。");
				return true;
				
				}
			}

			sender.sendMessage(LPOCore.banprefix + "/unban <PlayerName>");
		}
		}else {
			sender.sendMessage(LPOCore.warmprefix + ChatColor.RED + "You don't have Permission.");
		}
		return true;
	}

	@SuppressWarnings("deprecation")
	private String getUUID(String playername) {
		return Bukkit.getOfflinePlayer(playername).getUniqueId().toString();
	}
}
