package com.gmail.mame3619.lpo.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.manager.StatusManager;

public class VPCommands implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
			if(cmd.getName().equalsIgnoreCase("vp")){
				if(args.length == 1){
					String args0 = args[0];
					int coin = StatusManager.getVP(args0);
					sender.sendMessage(LPOCore.vpprefix + ChatColor.AQUA + args0 + "'s VP = " + ChatColor.WHITE +coin+ ChatColor.AQUA+ "vp(s)");
				}
				else {
					int coin = StatusManager.getCoin(sender.getName());
					sender.sendMessage(LPOCore.vpprefix + ChatColor.AQUA + "Your VP = " +ChatColor.WHITE +  coin +ChatColor.AQUA+ "vp(s)");
				}
			}
		return false;
	}

}
