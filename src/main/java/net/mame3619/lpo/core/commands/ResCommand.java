package com.gmail.mame3619.lpo.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import com.gmail.mame3619.lpo.core.manager.StatusManager;
import com.gmail.mame3619.lpo.core.task.RestartTask;


public class ResCommand implements CommandExecutor{
	
	public static Plugin plugin;
	
	public ResCommand(Plugin plugin) {
		ResCommand.plugin = plugin;
	}
	

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
			if(cmd.getName().equalsIgnoreCase("res")){
				String args0 = args[0];
				String args1 = args[1];
				String id = "none";
				String permission = StatusManager.getPermission(args1);
				if(permission.equalsIgnoreCase("Owner") || permission.equalsIgnoreCase("DeveloperPlus") || permission.equalsIgnoreCase("Administrator") || permission.equalsIgnoreCase("BuilderMaster")){
					if(permission.equalsIgnoreCase("Developer")){
						id = ChatColor.WHITE + "[" + ChatColor.AQUA + ChatColor.BOLD  + "Dev" +ChatColor.RESET + ChatColor.WHITE + "] " + args1;
					}
					else if(permission.equalsIgnoreCase("Owner")){
						id = ChatColor.WHITE + "[" + ChatColor.YELLOW + ChatColor.BOLD + "Owner" + ChatColor.RESET + ChatColor.WHITE + "] " + args1;
					}
					if(permission.equalsIgnoreCase("Administrator")){
						id = ChatColor.WHITE + "[" + ChatColor.RED + ChatColor.BOLD  + "Admin" +ChatColor.RESET + ChatColor.WHITE + "] " + args1;
					}
					else if(permission.equalsIgnoreCase("BuilderMaster")){
						id = ChatColor.WHITE + "[" + ChatColor.GREEN + ChatColor.BOLD + "BM" + ChatColor.RESET + ChatColor.WHITE + "] " + args1;
					}
					RestartTask.onStart(args0, id);
					}
				}else{
					sender.sendMessage("Unknown Command. Type �W/help�W for help.");
				}
		return false;
	}
}
