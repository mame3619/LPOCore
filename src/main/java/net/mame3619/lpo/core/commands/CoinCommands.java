package com.gmail.mame3619.lpo.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.manager.StatusManager;

public class CoinCommands implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
			if(cmd.getName().equalsIgnoreCase("coin")){
				if(args.length == 1){
					String args0 = args[0];
					int coin = StatusManager.getCoin(args0);
					sender.sendMessage(LPOCore.coinprefix + ChatColor.AQUA + args0 + "'s Coin = " + ChatColor.WHITE +coin+ ChatColor.AQUA+ "coin(s)");
				}
				else {
					int coin = StatusManager.getCoin(sender.getName());
					sender.sendMessage(LPOCore.coinprefix + ChatColor.AQUA + "Your Coin = " +ChatColor.WHITE +  coin +ChatColor.AQUA+ "coin(s)");
				}
			}
		return false;
	}

}
