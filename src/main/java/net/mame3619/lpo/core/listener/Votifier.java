package com.gmail.mame3619.lpo.core.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.gmail.mame3619.lpo.core.manager.StatusManager;
import com.vexsoftware.votifier.model.VotifierEvent;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.HoverEvent.Action;

public class Votifier implements Listener{
	
	@EventHandler
	public void onVote(VotifierEvent e){
		String voteplayer = e.getVote().getUsername();
		StatusManager.createPlayer(voteplayer);
			StatusManager.addVP(voteplayer, 1);
			Bukkit.broadcastMessage(ChatColor.BLUE + "Vote" + ChatColor.GRAY + "> " + ChatColor.RESET + voteplayer + "さんが投票してくれました。");
			Bukkit.broadcastMessage(ChatColor.BLUE + "Vote" + ChatColor.GRAY + "> " + ChatColor.RESET + "投票すると1vpもらえます。");
			TextComponent Vote = new TextComponent();
			TextComponent mae = new TextComponent();
			TextComponent usiro = new TextComponent();
			mae.setText(net.md_5.bungee.api.ChatColor.BLUE + "Vote" + net.md_5.bungee.api.ChatColor.GRAY + "> " + net.md_5.bungee.api.ChatColor.RESET + "投票は: ");
			usiro.setText("(クリックすることで投票ページに行けます。）");
			Vote.setText("こちら");
			Vote.setColor(net.md_5.bungee.api.ChatColor.GREEN);
			Vote.setBold(true);
			Vote.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("投票ページに行く").create()));
			Vote.setClickEvent(new ClickEvent(net.md_5.bungee.api.chat.ClickEvent.Action.OPEN_URL, "https://minecraft.jp/servers/5874a6d8a9b0bd0ba600834d/vote"));
			mae.addExtra(Vote);
			Vote.addExtra(usiro);
			for(Player player : Bukkit.getOnlinePlayers()){
				player.spigot().sendMessage(mae);
			}
	}



}
