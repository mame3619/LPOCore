package com.gmail.mame3619.lpo.core.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.defaults.BanCommand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.commands.BanCommands;
import com.gmail.mame3619.lpo.core.manager.BanManager;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class BotListener extends ListenerAdapter implements Listener{
	
	public static String chatChannelID = "280173952473759745";
	public static String informationChannelID = "296143505317953537";
	public static TextChannel chatchannel = null;
	public static TextChannel information = null;
	public static String ID = "";
	
    @Override
    public void onMessageReceived(MessageReceivedEvent e) {
        if (e.getChannel().getId().equalsIgnoreCase(chatChannelID)) {
        	if(e.getAuthor().isBot())return;
	        	String message = e.getMessage().getRawContent();
	        	String player =	e.getMember().getEffectiveName();
        	Bukkit.broadcastMessage(player + ChatColor.GREEN + ": " + ChatColor.RESET + message);
        }
    }
    
	public static void SendminecraftChattoDiscord(String message){
		chatchannel = LPOCore.jda.getTextChannelById(chatChannelID);
			chatchannel.sendMessage(message).queue();
		
	}
	
    public static void deleteMessage(Message message) {

        if (message.isFromType(ChannelType.PRIVATE)) return;

            message.delete().queue();


    }
	
	public static void banDiscord(String playername,String uuid ,String reason,String date, String longs ,String bannedby){
		information= LPOCore.jda.getTextChannelById(informationChannelID);
		information.sendMessage("=========BAN情報==============" + "\n"+ 
								"MCID: " + playername + "\n"+ 
								"UUID: " + uuid + "\n" +
								"理由: " + reason + "\n" +
								"期間: " + longs + "\n" +
								"行った運営: " + bannedby + "\n"+ 
								"BANに対する不服申し立てはFarron_Claireまで" + "\n"+
								"==============================").queue();
        new BukkitRunnable() {

            @Override

            public void run() {
        		BanManager.setDiscordID(playername, information.getLatestMessageId());;
            }
        }.runTaskLater(BanCommands.plugin, 20L);
	}
	
	public static void unbanDiscord(String playername,String unbandby,String DiscordID){

		information= LPOCore.jda.getTextChannelById(informationChannelID);
	information.sendMessage(playername + "のBANが" + unbandby +"によって解除されました。").queue();

	information.deleteMessageById(DiscordID).queue();
    new BukkitRunnable() {

        @Override

        public void run() {
        	ID = information.getLatestMessageId();
        }
    }.runTaskLater(BanCommands.plugin, 20L);
    new BukkitRunnable() {

        @Override

        public void run() {
        	information.deleteMessageById(ID).queue();
        }
    }.runTaskLater(BanCommands.plugin, 200L);
	}
 
 
}
