package com.gmail.mame3619.lpo.core.listener;



import javax.security.auth.login.LoginException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.manager.ScoreboardUtil;
import com.gmail.mame3619.lpo.core.manager.StatusManager;
import com.gmail.mame3619.lpo.core.util.IMEUtil;
import com.gmail.mame3619.lpo.core.util.KanaChenge;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.exceptions.RateLimitedException;

public class PlayerListener implements Listener{
	

	
	  public static Plugin plugin;
	
	  
	  public PlayerListener(Plugin plugin){
		    PlayerListener.plugin = plugin;  
		  }
	  
	public static String url = "none";
	

	@EventHandler
	public void onPlayerJ(PlayerJoinEvent e){
		Player player = e.getPlayer();
		e.setJoinMessage(ChatColor.AQUA +"" + ChatColor.BOLD + "Join" +ChatColor.RESET + " " + ChatColor.GRAY +"> " +ChatColor.RESET + player.getName());
		StatusManager.createPlayer(e.getPlayer().getName());
		if(url.equalsIgnoreCase("none"))return;
		player.setResourcePack(url);
	}
	
	@EventHandler
	public void onPlayerL(PlayerQuitEvent e){
		Player player = e.getPlayer();
		e.setQuitMessage(ChatColor.AQUA +"" + ChatColor.BOLD + "Quit" +ChatColor.RESET + " " + ChatColor.GRAY +"> " +ChatColor.RESET + player.getName());
	}
	

	@EventHandler
	public void noUproot(PlayerInteractEvent event)
	{
	    if(event.getAction() == Action.PHYSICAL && event.getClickedBlock().getType() == Material.SOIL)
	        event.setCancelled(true);
	}
	
	
	
	 



/*    @EventHandler
    public void toggle(PlayerInteractEvent event){
            final Player player = event.getPlayer();
            if (event.getAction() == Action.LEFT_CLICK_AIR){
                    new BukkitRunnable(){
                            Location loc = player.getLocation();
                            double t = 0;
                            double r = 2;
                            public void run(){
                                    t = t + Math.PI/16;
                                    double x = r*Math.cos(t);
                                    double y = r*Math.sin(t);
                                    double z = r*Math.sin(t);
                                    loc.add(x, y, z);
                                    ParticleEffect.WATER_SPLASH.send(Bukkit.getOnlinePlayers(),loc, 0, 0, 0, 0, 1);
                                    loc.subtract(x, y, z);
                            if (t > Math.PI*8){
                                    this.cancel();
                            }
                            }
                    }.runTaskTimer(plugin, 0, 1);
            }
    }
	
	*/
	
	 @EventHandler(ignoreCancelled=true, priority=EventPriority.LOW)
	  public void onConstFlight(PlayerMoveEvent event)
	  {
	    Player player = event.getPlayer();
	    if (!player.isGliding()) {
	      return;
	    }
	    Vector vector = player.getVelocity();
	    if (vector.length() > 1D) {
	      return;
	    }
	    player.setVelocity(vector.normalize().multiply(1D));
	  }
	 
	 
    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e){
    	
    	String id = e.getPlayer().getName();
    	String permission = StatusManager.getPermission(id);
    	String message = e.getMessage();
    	message = replaceColorCode(message);
    	String kana = "";
    	kana = KanaChenge.conv(message);
    	String jpmessage = IMEUtil.convByGoogleIME(kana);
    	boolean skipJapanize = false;
    	String kanaTemp = this.stripColor(message);
    	if(StatusManager.getKanaSkip(id).equalsIgnoreCase("true")){
    		skipJapanize = true;
    	}
    	else if ( !skipJapanize && ( kanaTemp.getBytes().length > kanaTemp.length() || kanaTemp.matches("[ \\uFF61-\\uFF9F]+") ) ) {
            skipJapanize = true;
        }

    	if(skipJapanize){
    		e.setMessage(message);
    	}
    	else if(!skipJapanize){
    	e.setMessage(jpmessage + ChatColor.GOLD + " (" + message + ")");
    	}
    	String mess = e.getMessage();
	        if(permission.equalsIgnoreCase("DeveloperPlus")){
	       	e.setFormat(ChatColor.GRAY + "["  +ChatColor.AQUA + ChatColor.BOLD + "Dev+" + ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	       	
	        }
	        if(permission.equalsIgnoreCase("Developer")){
	       	e.setFormat(ChatColor.GRAY + "["  +ChatColor.AQUA +  "Dev" + ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	       	
	        }
	        if(permission.equalsIgnoreCase("Owner")){
	    	e.setFormat(ChatColor.GRAY +"["+ChatColor.YELLOW + ChatColor.BOLD + "Owner" + ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	        
	    	
	        }
	        if(permission.equalsIgnoreCase("Administrator")){
	    	e.setFormat(ChatColor.GRAY +"["+ChatColor.RED + ChatColor.BOLD+"Admin" + ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	        
	    	
	        }
	        if(permission.equalsIgnoreCase("BuilderMaster")){
	    	e.setFormat(ChatColor.GRAY +"[" + ChatColor.GREEN +ChatColor.BOLD + "BM" + ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	    	
	        }
	        if(permission.equalsIgnoreCase("Moderator")){
	    	e.setFormat(ChatColor.GRAY +"[" + ChatColor.GOLD +"Mod" + ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	        
	    	
	        }
	        if(permission.equalsIgnoreCase("BuilderPlus")){
	    	e.setFormat(ChatColor.GRAY +"[" + ChatColor.DARK_GREEN +"Builder+"+ ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	    	
	        }
	        if(permission.equalsIgnoreCase("YouTube")){
	    	e.setFormat(ChatColor.GRAY +"[" + ChatColor.DARK_GREEN +"YT" + ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	        
	    	
	        }
	        if(permission.equalsIgnoreCase("Builder")){
	    	e.setFormat(ChatColor.GRAY +"[" + ChatColor.GREEN +"Builder" + ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	    	
	        }
	        if(permission.equalsIgnoreCase("Helper")){
	    	e.setFormat(ChatColor.GRAY +"[" + ChatColor.LIGHT_PURPLE +"Helper" + ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	    	
	        }
	        if(permission.equalsIgnoreCase("TextureCreator")){
	    	e.setFormat(ChatColor.GRAY +"[" + ChatColor.YELLOW +"TC" + ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	    	
	        }
	        if(permission.equalsIgnoreCase("SpecialThanks")){
	    	e.setFormat(ChatColor.GRAY +"[" + ChatColor.DARK_PURPLE +"ST" + ChatColor.RESET +  ChatColor.GRAY +"]"+ ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	    	
	        }
	        if(permission.equalsIgnoreCase("Member")){
	        e.setFormat(ChatColor.RESET + "%s"+ ChatColor.GREEN + ": " + ChatColor.RESET + "%s");
	        
	        }
	    	if(skipJapanize){
	        	BotListener.SendminecraftChattoDiscord(id+ ": " +  message); 
	    	}
	    	else if(!skipJapanize){
	        	BotListener.SendminecraftChattoDiscord(id+ ": " + jpmessage+"(" + message + ")"); 
	    	}
       
    }
    
    public String stripColor(String source) {
        if (source == null)
            return null;
        return ChatColor.stripColor(source);
    }
	
    public static String replaceColorCode(String source) {
        if (source == null)
            return null;
        return ChatColor.translateAlternateColorCodes('&', source);
    }

    
    
}
