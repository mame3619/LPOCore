package com.gmail.mame3619.lpo.core.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import com.gmail.mame3619.lpo.core.manager.BanManager;

public class BanListener implements Listener{
	
	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent e){
		Player player = e.getPlayer();
		if(BanManager.isBanned(player.getUniqueId().toString())){
		long current = System.currentTimeMillis();
		long end = BanManager.getEnd(player.getUniqueId().toString());
			if(current < end | end == -1) {
				e.disallow(Result.KICK_BANNED, ChatColor.RED + "" + ChatColor.BOLD + "あなたはBANされています\n"+
						"\n" +
						ChatColor.RESET + ChatColor.AQUA + "理由: "+ ChatColor.RESET + BanManager.getReason(player.getUniqueId().toString())+ "\n"+
						"\n" +
													ChatColor.AQUA + "期限: " +ChatColor.RESET +  BanManager.getRemainingTime(player.getUniqueId().toString()) + "\n"+
						"\n" +
						ChatColor.AQUA + "BANした運営: " + ChatColor.RESET + BanManager.getBannedby(player.getUniqueId().toString()) + 
						"\n\n" +
						ChatColor.RESET+ "BANされた日時: "+ BanManager.getDate(player.getUniqueId().toString()) + 
						"\n\n" + 
						ChatColor.BOLD + ""  + ChatColor.RED +"不当なBANの場合BAN解除申請を行うことができます。");
			}
		}
		
	}

}
