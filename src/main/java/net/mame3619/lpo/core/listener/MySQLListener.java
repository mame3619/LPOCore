package com.gmail.mame3619.lpo.core.listener;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.manager.StatusManager;

public class MySQLListener implements Listener{
	
	@EventHandler
	public static void onPlayerLogin(PlayerLoginEvent e){
		if(!LPOCore.Connect){
			e.disallow(null, ChatColor.RED + "[Error] データベースとの接続を確立できませんでした。\n再度ログインし、接続できない場合は運営へ連絡をお願い致します。");
		}
	}
	
	@EventHandler
	public static void onPlayerJoin(PlayerJoinEvent e){
		String id = e.getPlayer().getName();
		StatusManager.createPlayer(id);
	}

}
