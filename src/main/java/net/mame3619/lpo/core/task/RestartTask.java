package com.gmail.mame3619.lpo.core.task;

import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.gmail.mame3619.lpo.core.LPOCore;
import com.gmail.mame3619.lpo.core.commands.SystemCommands;


public class RestartTask{
	static int time = 10;
	static int count;
	  
	public static void onStart(String reason, String sender){
			
	
	
		count = Bukkit.getScheduler().scheduleSyncRepeatingTask(SystemCommands.plugin, new Runnable(){
			public void run(){
				if (RestartTask.time == 10) {
					Bukkit.broadcastMessage(ChatColor.GRAY + "------------------------------" + ChatColor.RED+ ChatColor.BOLD + "再起動警告"+ ChatColor.RESET + ChatColor.GRAY + "------------------------------\n" +
				    LPOCore.warmprefix + ChatColor.RED +ChatColor.BOLD + "今から10秒後に再起動を行います。プレイヤーは自動的にログアウトされます。\n"+
					LPOCore.warmprefix + ChatColor.RED+ ChatColor.BOLD + "理由: " + ChatColor.WHITE + reason + "\n"+
				    LPOCore.warmprefix + ChatColor.RED + ChatColor.BOLD + "実施者: " + ChatColor.WHITE + sender + "\n" +
				    ChatColor.RESET + ChatColor.GRAY + "------------------------------------------------------------------------");
				}
				if (RestartTask.time == 5) {
					Bukkit.broadcastMessage(LPOCore.warmprefix + ChatColor.RED + ChatColor.BOLD + "再起動5秒前");
				}
				if (RestartTask.time == 4) {
					Bukkit.broadcastMessage(LPOCore.warmprefix + ChatColor.RED + ChatColor.BOLD + "再起動4秒前");
				}
				if (RestartTask.time == 3) {
					Bukkit.broadcastMessage(LPOCore.warmprefix + ChatColor.RED + ChatColor.BOLD + "再起動3秒前");
				}
				if (RestartTask.time == 2) {
					Bukkit.broadcastMessage(LPOCore.warmprefix + ChatColor.RED + ChatColor.BOLD + "再起動2秒前");
				}
				if (RestartTask.time == 1) {
					Bukkit.broadcastMessage(LPOCore.warmprefix + ChatColor.RED + ChatColor.BOLD + "再起動1秒前");
				}
				if (RestartTask.time > 0){
					RestartTask.time -= 1;
				}else{
					Bukkit.getScheduler().cancelTask(RestartTask.count);
					RestartTask.time = 10;
					onResult();
				}
			}
		}, 0L, 20L);
	}
		  
	public static void onResult(){
		Calendar calendar = Calendar.getInstance();
		int year  = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DATE);
	    int hour = calendar.get(Calendar.HOUR_OF_DAY);
	    int minute = calendar.get(Calendar.MINUTE);
	    int second = calendar.get(Calendar.SECOND);
		    String date = year + "/" + month + "/" + day + "  " + hour + ":" + minute + "." + second;
		for(Player player : Bukkit.getOnlinePlayers()){
			player.kickPlayer("再起動を行いました\n\n\n\n\n"+ "                          " + date);
		}
		Bukkit.getServer().shutdown();
	}

}
  
