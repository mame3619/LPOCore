package com.gmail.mame3619.lpo.core;

import java.io.File;
import java.io.IOException;

import javax.security.auth.login.LoginException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.mame3619.lpo.core.commands.BanCommands;
import com.gmail.mame3619.lpo.core.commands.CoinCommands;
import com.gmail.mame3619.lpo.core.commands.GiveCoinCommand;
import com.gmail.mame3619.lpo.core.commands.GiveVPCommand;
import com.gmail.mame3619.lpo.core.commands.OpenPlayerMenu;
import com.gmail.mame3619.lpo.core.commands.ResCommand;
import com.gmail.mame3619.lpo.core.commands.SendCoinCommand;
import com.gmail.mame3619.lpo.core.commands.SendVPCommand;
import com.gmail.mame3619.lpo.core.commands.SystemCommands;
import com.gmail.mame3619.lpo.core.commands.VPCommands;
import com.gmail.mame3619.lpo.core.listener.BanListener;
import com.gmail.mame3619.lpo.core.listener.BotListener;
import com.gmail.mame3619.lpo.core.listener.MySQLListener;
import com.gmail.mame3619.lpo.core.listener.PlayerListener;
import com.gmail.mame3619.lpo.core.listener.Votifier;
import com.gmail.mame3619.lpo.core.manager.FileManager;
import com.gmail.mame3619.lpo.core.menu.PlayerMenu;
import com.gmail.mame3619.lpo.core.menu.TeleportMenu;
import com.gmail.mame3619.lpo.core.util.MySQL;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.minecraft.server.v1_11_R1.PlayerList;


public class LPOCore extends JavaPlugin{
	
    public static JDA jda;
    
    public static final String BOT_TOKEN = "MjgyNTAzODc2ODAzMDM1MTM2.C-n4Dw.mWsgtjbeIVtjQIr-MYPYQ3ITv2I";
 
	public static String prefix = ChatColor.RESET + "" +ChatColor.AQUA + "" +ChatColor.BOLD + "LPOCore" + ChatColor.GRAY + "> " +ChatColor.RESET ;
	public static String systemprefix =  ChatColor.RESET + "" +ChatColor.GOLD +""+  ChatColor.BOLD +  "System" + ChatColor.GRAY + "> " +ChatColor.RESET ;
	public static String warmprefix =  ChatColor.RESET + "" +ChatColor.DARK_RED +""+  ChatColor.BOLD +  "WARNING" + ChatColor.GRAY + "> " +ChatColor.RESET ;
	public static String infoprefix = ChatColor.RESET + "" +ChatColor.YELLOW +"" +  ChatColor.BOLD + "Info" + ChatColor.GRAY + "> " + ChatColor.RESET;
	public static String coinprefix = ChatColor.RESET + "" + ChatColor.GREEN + ChatColor.BOLD + "Coin" + ChatColor.GRAY + "> " + ChatColor.RESET;	
	public static String vpprefix = ChatColor.RESET + "" + ChatColor.GREEN + ChatColor.BOLD + "VP" + ChatColor.GRAY + "> " + ChatColor.RESET;	
	public static String banprefix = ChatColor.RESET + "" + ChatColor.DARK_PURPLE + ChatColor.BOLD + "Ban" + ChatColor.GRAY + "> " + ChatColor.RESET;	
	public static boolean Connect = false;

	@Override
	public void onEnable() {
		Bukkit.getConsoleSender().sendMessage(prefix + "Enable LPOCore");
		FileManager.setStanderdMySQL();
		FileManager.readMySQL();
		MySQL.connect();
		MySQL.CreateTable();
		registerCommands();
		registerEvents();
		new Thread(this::init,"LPOCore - Initialization").start();
	}
	
	
	   public void init() {
		   if (jda != null) try { jda.shutdown(false); } catch (Exception e) { e.printStackTrace(); }
	        try {
	            jda = new JDABuilder(AccountType.BOT).addEventListener(new BotListener()).setToken(BOT_TOKEN).buildBlocking();
	            jda.setAutoReconnect(true);
	        } catch (LoginException | IllegalArgumentException | InterruptedException | RateLimitedException e) {
	            e.printStackTrace();
	        }
	        
	   }
	@Override 
	public void onDisable() {
        if (jda != null) jda.getPresence().setStatus(OnlineStatus.INVISIBLE);
        if (jda != null) jda.shutdown(false);
	}
	
	private void registerCommands() {
		getCommand("system").setExecutor(new SystemCommands(this));
		getCommand("res").setExecutor(new ResCommand(this));
		getCommand("coin").setExecutor(new CoinCommands());
		getCommand("sendcoin").setExecutor(new SendCoinCommand());
		getCommand("givecoin").setExecutor(new GiveCoinCommand());
		getCommand("vp").setExecutor(new VPCommands());
		getCommand("givevp").setExecutor(new GiveVPCommand());
		getCommand("sendvp").setExecutor(new SendVPCommand());
		getCommand("ban").setExecutor(new BanCommands(this));
		getCommand("check").setExecutor(new BanCommands(this));
		getCommand("unban").setExecutor(new BanCommands(this));
		getCommand("menu").setExecutor(new OpenPlayerMenu());
		
		
		
	}

	private void registerEvents() {
		Bukkit.getServer().getPluginManager().registerEvents(new BanListener(),this);
		Bukkit.getServer().getPluginManager().registerEvents(new MySQLListener(),this);
		Bukkit.getServer().getPluginManager().registerEvents(new Votifier(),this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerMenu(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new TeleportMenu(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new BotListener(), this);
	}
	

}
